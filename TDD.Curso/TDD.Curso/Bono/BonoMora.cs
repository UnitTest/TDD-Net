﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDD.Curso.Bono
{
    public class BonoMora : IReglaBono
    {
        public double Bono(EmpleadoResumen empleado)
        {
            double bono = 0.00;
            if (empleado.Mora >= 0.5) bono += 1000;
            return bono;
        }

    }
}
