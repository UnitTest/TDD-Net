﻿namespace TDD.Curso.Bono
{
    public class BonoNuevosClientes : IReglaBono
    {
        public double Bono(EmpleadoResumen empleado)
        {
            double bono = 0.00;
            if (empleado.ClientesNuevos < 10) return bono;
            bono += 1000;
            if (empleado.ClientesNuevos > 10) bono += 500;
            return bono;
        }
    }
}