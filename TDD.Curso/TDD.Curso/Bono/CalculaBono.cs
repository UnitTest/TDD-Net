﻿using System.Collections.Generic;

namespace TDD.Curso.Bono
{
    public class CalculaBono
    {
        private double totalBono = 0;
        private readonly IEnumerable<IReglaBono> bonosAAplicar;

        public CalculaBono(IEnumerable<IReglaBono> bonosAAplicar )
        {
            this.bonosAAplicar = bonosAAplicar;
        }

        public void Calcular(List<EmpleadoResumen> empleados)
        {
            foreach (var empleado in empleados)
            {
                var bono = 0.00;
                foreach (var bonoAAplicar in bonosAAplicar)
                {
                    bono += bonoAAplicar.Bono(empleado);    
                }

                empleado.Bono = bono;
                totalBono += bono;
            }
        }

        public double TotalBono()
        {
            return totalBono;
        }
    }

}
