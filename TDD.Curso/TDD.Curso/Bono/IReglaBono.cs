﻿namespace TDD.Curso.Bono
{
    public interface IReglaBono
    {
        double Bono(EmpleadoResumen empleado);
    }
}