﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDD.Curso.Bono
{
    public class BonoRetension : IReglaBono
    {
        public double Bono(EmpleadoResumen empleado)
        {
            double bono = 0.00;
            if (!(empleado.Retension >= 0.5)) return bono;
            bono += 800;
            if (empleado.Retension > 0.50) bono += 400;
            return bono;

        }
    }
}
