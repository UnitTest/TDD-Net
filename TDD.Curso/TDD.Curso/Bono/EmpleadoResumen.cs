﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDD.Curso.Bono
{
    public class EmpleadoResumen
    {
        public int ClientesNuevos { get; set; }

        public double Retension { get; set; }

        public double Bono { get; set; }

        public double Mora { get; set; }
    }
}
