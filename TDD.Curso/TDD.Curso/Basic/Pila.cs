﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace TDD.Curso
{
    public class Pila
    {
        private IList<string> elementos;
        private readonly int totalElementos;

        public Pila(int totalElementos)
        {
            this.totalElementos = totalElementos;
            elementos = new List<string>();
        }

        public bool Vacia()
        {
            return (elementos.Count==0);
        }

        public string Sacar()
        {
            if (this.Vacia()) throw new InvalidOperationException("Pila Vacia!!!");

            var elemento = elementos[IndiceUltimoElemento()];
            elementos.RemoveAt(IndiceUltimoElemento());
            return elemento;
        }

        public void Insertar(string elemento)
        {
            if (PilaLlena()) throw new InvalidOperationException("Pila Llena!!!");
            this.elementos.Add(elemento);
        }

        private bool PilaLlena()
        {
            return elementos.Count>= totalElementos;
        }

        private int IndiceUltimoElemento()
        {
            return elementos.Count - 1;
        }
    }
}
