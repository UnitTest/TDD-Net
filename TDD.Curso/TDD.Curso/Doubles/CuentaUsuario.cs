﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDD.Curso.Doubles
{
    public class CuentaUsuario
    {
        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string Password { get; set; }
        public int Id { get; set; }
    }
}
