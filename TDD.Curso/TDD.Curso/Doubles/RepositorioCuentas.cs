﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDD.Curso.Doubles
{
    public interface IRepositorioCuentas
    {
        CuentaUsuario Grabar(CuentaUsuario cuentaUsuario);
    }

    public class RepositorioCuentas : IRepositorioCuentas
    {
        public CuentaUsuario Grabar(CuentaUsuario cuentaUsuario)
        {
            //TODO implementar acceso a datos
            cuentaUsuario.Id = 1;
            return cuentaUsuario;
        }
    }
}
