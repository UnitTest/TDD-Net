﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TDD.Curso.Doubles
{
    public class AdministradorCuentas
    {
        private IRepositorioCuentas cuentasRepositorio;

        public AdministradorCuentas(IRepositorioCuentas repositorio)
        {
            cuentasRepositorio = repositorio;  // creaba explicita la instancia del repositorio
        }

        public CuentaUsuario Grabar(CuentaUsuario cuentaUsuario)
        {
            CuentaUsuario cuentaUsuarioNueva = new CuentaUsuario();
            if (string.IsNullOrEmpty(cuentaUsuario.Nombre)) throw new InvalidDataException();
            var intentos = 0;
            while (intentos<3)
            {
                try
                {
                    cuentaUsuarioNueva= cuentasRepositorio.Grabar(cuentaUsuario);
                    intentos = 4;
                }
                catch (TimeoutException ex)
                {
                    Console.WriteLine(ex);
                    intentos++;
                }
            }
            return cuentaUsuarioNueva;
        }
    }
}
