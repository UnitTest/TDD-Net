﻿using System;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;

namespace TDD.Api
{
    public class ActivadorDeControllers: IHttpControllerActivator
    {
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            return new AutenticacionController(new AutenticacionProveedorBasico());
        }
    }
}
