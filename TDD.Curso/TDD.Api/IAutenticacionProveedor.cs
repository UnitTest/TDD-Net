﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TDD.Api
{
    public interface IAutenticacionProveedor
    {
        bool ValidaUsuario(string nombreUsuario, string contraseña);
    }
}
