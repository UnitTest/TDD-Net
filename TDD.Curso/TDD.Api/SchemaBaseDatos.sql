﻿IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name= N'Autenticacion')
DROP DATABASE [Autenticacion]

CREATE DATABASE [Autenticacion]
GO

USE [Autenticacion]
GO

CREATE TABLE [dbo].[Usuarios](
	[UserName] [nvarchar](30) NOT NULL PRIMARY KEY,
	[Password] [nvarchar](100) NOT NULL 
)
GO