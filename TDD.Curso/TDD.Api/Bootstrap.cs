﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace TDD.Api
{
    public class Bootstrap
    {
        public void Configure(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "ApiDefault",
                routeTemplate: "{controller}/{id}",
                defaults: new
                {
                    controller = "Autenticacion",
                    id = RouteParameter.Optional
                }
                );

            config.Services.Replace(typeof(IHttpControllerActivator),new ActivadorDeControllers());
        }

        public void InstalarBaseDatos()
        {
            var connStr = ConfigurationManager.ConnectionStrings["autenticacion"].ConnectionString;
            var builder = new SqlConnectionStringBuilder(connStr);
            builder.InitialCatalog = "Master";

            using (var conn = new SqlConnection(builder.ConnectionString))
            {
                conn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    var schemaSql = Resource1.SchemaBaseDatos;

                    foreach (var sql in 
                        schemaSql
                            .Split(
                                new []{"GO"},
                                StringSplitOptions.RemoveEmptyEntries
                                ))
                    {
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }

            }
        }

        public void DesInstalarBaseDatos()
        {
            var connStr = ConfigurationManager.ConnectionStrings["autenticacion"].ConnectionString;
            var builder = new SqlConnectionStringBuilder(connStr);
            builder.InitialCatalog = "Master";
            using (var conn = new SqlConnection(builder.ConnectionString))
            {
                conn.Open();
                var eliminarBD = @"
                        IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name= N'Autenticacion')
                        DROP DATABASE [Autenticacion];";
                using (var cmd = new SqlCommand(eliminarBD, conn))
                    cmd.ExecuteNonQuery();
            }
        }
    }
}
