﻿using System.ComponentModel;
using System.Configuration;
using Simple.Data;

namespace TDD.Api
{
    public class AutenticacionProveedorBasico : IAutenticacionProveedor
    {
        private dynamic bd;

        public AutenticacionProveedorBasico()
        {
            var connStr = ConfigurationManager.ConnectionStrings["autenticacion"].ConnectionString;
            bd = Database.OpenConnection(connStr);
        }
        public bool ValidaUsuario(string nombreUsuario, string contraseña)
        {
            var usuario = bd.Usuarios.Find(bd.Usuarios.UserName == nombreUsuario);
            
            return (usuario.Password == contraseña);
        }
    }
}