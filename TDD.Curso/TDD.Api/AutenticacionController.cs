﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TDD.Api
{
    public class AutenticacionController : ApiController
    {
        private IAutenticacionProveedor autenticacionProveedor;

        public AutenticacionController(IAutenticacionProveedor autenticacionProveedor)
        {
            this.autenticacionProveedor = autenticacionProveedor;
        }
        public IHttpActionResult Get()
        {
            return Ok();
        }

        public HttpResponseMessage Post(LoginModel login)
        {
            HttpStatusCode codigo = HttpStatusCode.OK;
            
            var resultado = autenticacionProveedor.ValidaUsuario(login.UserName, login.Password);
            
            if (!resultado) codigo = HttpStatusCode.BadRequest;
            
            var response = this.Request.CreateResponse(codigo);
            
            response.Headers.Add("Authorization","Bearer Token");
            return response;
        } 
    }
}