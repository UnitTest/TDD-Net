﻿using System;
using System.IO;
using NUnit.Framework;
using FluentAssertions;
using TDD.Curso.Doubles;
using Moq;

namespace TDD.Curso.Tests
{
    public class TestDoublesDemo
    {
        private Mock<IRepositorioCuentas> repositorioCuentasMock;
        private CuentaUsuario cuentaUsuario;

        [SetUp]
        public void ConfigurarTest()
        {
            repositorioCuentasMock = new Mock<IRepositorioCuentas>();
            cuentaUsuario = new CuentaUsuario() { Nombre = "Uzi", Apellido = "Mamani", Password = "123456789" };
        }

        [TearDown]
        public void LimpiarDataTest()
        {
            cuentaUsuario = null;
            repositorioCuentasMock = null;
        }

        [Test]
        public void Deberia_Grabar_Cuenta()
        {
            var nuevoUsuario = cuentaUsuario;
            nuevoUsuario.Id = 1;
            repositorioCuentasMock.Setup(m => m.Grabar(cuentaUsuario)).Returns(nuevoUsuario);
            var cuentas = new AdministradorCuentas(repositorioCuentasMock.Object);
            var usuario = cuentas.Grabar(cuentaUsuario);
            usuario.Id.Should().BeGreaterThan(0);
        }


        [Test]
        [ExpectedException(typeof(InvalidDataException))]
        public void No_Deberia_Grabar_Cuenta_Sin_Nombre()
        {
            var cuentas = new AdministradorCuentas(repositorioCuentasMock.Object);
            cuentaUsuario.Nombre = string.Empty;
            var usuario = cuentas.Grabar(cuentaUsuario);
        }

        [Test]
        public void Si_Hay_Un_TimeOut_Debe_Reprocesar_3_Veces()
        {
            repositorioCuentasMock.Setup(m => m.Grabar(It.IsAny<CuentaUsuario>())).Throws<TimeoutException>();
            var cuentas = new AdministradorCuentas(repositorioCuentasMock.Object);

            var usuario = cuentas.Grabar(cuentaUsuario);
            repositorioCuentasMock.Verify(m=>m.Grabar(It.IsAny<CuentaUsuario>()),Times.Exactly(3));
        }

    }
}