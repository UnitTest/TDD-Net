﻿using System;
using System.Net.Http;
using System.Web.Http.SelfHost;
using TDD.Api;

namespace TDD.Curso.Tests
{
    public class ClienteFabrica
    {
        public static HttpClient Crear()
        {
            var urlBase = new Uri("http://localhost:9874");
            var config = new HttpSelfHostConfiguration(urlBase);
            var server = new HttpSelfHostServer(config);
            new Bootstrap().Configure(config);
            var client = new HttpClient(server);
            try
            {
                client.BaseAddress = urlBase;
                return client;
            }
            catch (Exception e)
            {
                client.Dispose();
                throw;
            }
        }
    }
}