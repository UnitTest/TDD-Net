﻿using System;
using FluentAssertions;
using NUnit.Framework;
using TDD.Curso;

namespace TDD.Curso.Tests
{
    [TestFixture]
    public class PilaTest
    {
        private Pila pila;
        [SetUp] // hace que este met. se ejecute siempre antes de cada test
        public void Setup()
        {
            pila = new Pila(10);
        }

        [TearDown] // hace que este met. se ejecute siempre despues de cada test
        public void Teardown()
        {
            pila = null;
        }

        [Test]
        public void CreoUnaPila_LaPila_DebeEstarVacia()
        {
            var estado = pila.Vacia(); //Act -> Ejecutar el codigo/accion que se va probar
            estado.Should().Be(true);  // Assert -> la verficacion/validación
        }


        [Test]
        [ExpectedException(typeof(InvalidOperationException),ExpectedMessage = "Pila Vacia!!!")]
        public void SacarElementoDeunaPilaVacia_DebeLanzarUnaException()
        {
            var elemento = pila.Sacar();
        }

        [Test]
        public void InsertarUnElementoEnLaPila_LaPilaNoDebeEstarVacia()
        {
            pila.Insertar("Elemento1");
            pila.Vacia().Should().Be(false);
        }

        [Test]
        public void InsertarYSacarUnElementoDeLaPIla_LaPilaDebeQuedarVacia()
        {
            pila.Insertar("Elemento1");
            pila.Sacar();
            pila.Vacia().Should().Be(true);
        }

        [Test]
        public void InsertoDosElementos_SacoUnElemento_ElementoRetornadoDeberaSerElUltimo()
        {
            pila.Insertar("Elemento1");
            pila.Insertar("Elemento2");

            var elemento = pila.Sacar();

            elemento.Should().Be("Elemento2");
        }

        [Test]
        [ExpectedException(typeof(InvalidOperationException),ExpectedMessage = "Pila Llena!!!")]
        public void DadaUnaPIlaDe2Elementos_CuandoIntentoInsertarEl3erElemento_LanzaExcepcionPilaLlena()
        {
            var pila = new Pila(2);
            pila.Insertar("Elemento1");
            pila.Insertar("Elemento2");
            pila.Insertar("Elemento3");
        }

        [Test]
        public void InsertarELementos_YSacarlosElementos_DebemosRecibirlosEnElOrderCorrecto()
        {
            pila.Insertar("Elemento1");
            pila.Insertar("Elemento2");
            pila.Insertar("Elemento3");

            pila.Sacar().Should().Be("Elemento3");
            pila.Sacar().Should().Be("Elemento2");
            pila.Sacar().Should().Be("Elemento1");
            pila.Vacia().Should().Be(true);

        }


    }
}
