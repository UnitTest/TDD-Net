﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using TDD.Curso.Bono;

namespace TDD.Curso.Tests
{
    [TestFixture]
    public class CalculaBonoTest
    {
        private CalculaBono calculaBono;
        private List<EmpleadoResumen> listaEmpleados;

        [SetUp]
        public void ConfiguraPruebas()
        {
            calculaBono = new CalculaBono(bonosAAplicar());
            listaEmpleados = new List<EmpleadoResumen>();
        }

        [TearDown]
        public void LimpiarPruebas()
        {
            calculaBono = null;
            listaEmpleados = null;
        }

        [Test]
        public void DadaListaDeEmpleadoVacia_TotalBono_DebeSerCero()
        {
            calculaBono.Calcular(listaEmpleados);
            var totalBono = calculaBono.TotalBono();
            totalBono.Should().Be(0, "Bono total deberia ser Cero");
        }

        [Test]
        public void DadaUnEmpleadoQueNollegoASuMeta_TotalBono_DebeSerCero()
        {
            listaEmpleados.Add(vendedorNoLlegaASuMeta());

            calculaBono.Calcular(listaEmpleados);
            var totalBono = calculaBono.TotalBono();
            totalBono.Should().Be(0, "Bono total deberia ser Cero");
        }

        [Test]
        public void DadaUnVendedorQueNollegoASuMeta_TotalBono_DebeSer1800()
        {
            listaEmpleados.Add(vendedorLlegaASuMeta());

            calculaBono.Calcular(listaEmpleados);
            var totalBono = calculaBono.TotalBono();
            totalBono.Should().Be(1800, "Bono total deberia ser Cero");
        }

        [Test]
        public void DadaUnVendedorQueSuperaSuMeta_TotalBono_DebeSer2700()
        {
            listaEmpleados.Add(vendedorSuperaMeta());
            calculaBono.Calcular(listaEmpleados);
            var totalBono = calculaBono.TotalBono();
            totalBono.Should().Be(2700, "Bono total deberia ser Cero");
        }

        [Test]
        public void DadaUnCobradorQueSuperaSuMeta_TotalBono_DebeSer1000()
        {
            listaEmpleados.Add(cobradorLLegaAsuMeta());

            calculaBono.Calcular(listaEmpleados);
            var totalBono = calculaBono.TotalBono();
            totalBono.Should().Be(1000, "Bono total deberia ser Cero");
        }

        [Test]
        public void DadaUnCobradorYUnVendedorQueLleganSuMeta_TotalBono_DebeSer2800()
        {
            listaEmpleados.Add(cobradorLLegaAsuMeta());
            listaEmpleados.Add(vendedorLlegaASuMeta());

            calculaBono.Calcular(listaEmpleados);
            var totalBono = calculaBono.TotalBono();
            totalBono.Should().Be(2800, "Bono total deberia ser Cero");
        }

        
        #region helpers
        private IEnumerable<IReglaBono> bonosAAplicar()
        {
            var bonos = new List<IReglaBono>()
            {
                new BonoMora(),
                new BonoNuevosClientes(),
                new BonoRetension()
            };
            return bonos;
        }

        private EmpleadoResumen cobradorLLegaAsuMeta()
        {
            return new EmpleadoResumen
                {
                    Mora = 0.5
                };

        }

        private EmpleadoResumen vendedorLlegaASuMeta()
        {
            return new EmpleadoResumen
            {
                ClientesNuevos = 10,
                Retension = 0.5
            };
        }

        private EmpleadoResumen vendedorSuperaMeta()
        {
            return new EmpleadoResumen
            {
                ClientesNuevos = 13,
                Retension = 0.6
            };

        }

        private EmpleadoResumen vendedorNoLlegaASuMeta()
        {
            return new EmpleadoResumen
            {
                ClientesNuevos = 0,
                Retension = 0
            };

        }

        #endregion

        
    }
}