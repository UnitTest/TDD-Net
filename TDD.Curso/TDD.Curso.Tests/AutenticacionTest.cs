﻿using System;
using System.Configuration;
using NUnit.Framework;
using FluentAssertions;
using System.Net.Http;
using System.Web.Http.SelfHost;
using Simple.Data;
using TDD.Api;

namespace TDD.Curso.Tests
{
    public class AutenticacionTest
    {
        [SetUp]
        public void ConfiguraBaseDatos()
        {
            new Bootstrap().InstalarBaseDatos();
            var connStr = ConfigurationManager.ConnectionStrings["autenticacion"].ConnectionString;
            dynamic bd = Database.OpenConnection(connStr);

            bd.Usuarios.Insert(UserName: "jperez@acme.pe", Password: "123456789!");
            bd.Usuarios.Insert(UserName: "umamani@acme.pe", Password: "123456!");
            
        }

        [TearDown]
        public void EliminaBaseDatos()
        {
            new Bootstrap().DesInstalarBaseDatos();
        }

        [Test]
        public void DebemosRecibirUnaRespuestaExitosa()
        {
            using (var client = ClienteFabrica.Crear())
            {
                var respuesta = client.GetAsync("").Result;
                respuesta.IsSuccessStatusCode.Should().BeTrue();
            }
        }

        [Test]
        [TestCase("jperez@acme.pe", "123456789!",true)]
        [TestCase("jperez@acme.pe", "9876544", false)]
        [TestCase("umamani@acme.pe", "123456!", true)]
        public void CuandoPosteoUnLoginDeborecibirUnOk(string usuario, string password, bool resultado)
        {
            using (var client = ClienteFabrica.Crear())
            {
                var jsonLogin = new
                {
                    userName = usuario,
                    password = password
                };

                var respuesta = client.PostAsJsonAsync("", jsonLogin).Result;

                respuesta
                    .IsSuccessStatusCode
                    .Should().Be(resultado,
                        string.Format("Codigo de respuesta {0} mensaje {1}", respuesta.StatusCode,
                            respuesta.ReasonPhrase));

                respuesta.Headers.Contains("Authorization").Should().BeTrue("Debe tener un Header de Authotizacion");
            }
        }
    }
}